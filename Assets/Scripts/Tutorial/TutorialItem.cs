using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Шаг обучалки
/// </summary>
[Serializable]
public class TutorialItem
{
    [SerializeField]
    private int level;

    public int Level => level;

    [SerializeField]
    private string windowTypeName;

    [SerializeField]
    private TutorialWindowMode tutorialWindowMode;
    
    [SerializeField]
    private int time = 3;

    public int Time => time;

    [SerializeField]
    private RectTransform target;

    public RectTransform Target => target;

    [SerializeField]
    private bool waitEvent;
    
    [SerializeField]
    private bool waitTargetChanged;

    [SerializeField]
    private string text;

    public string Text => text;

    [SerializeField]
    private bool showTextInTheEnd;

    public bool ShowTextInTheEnd => showTextInTheEnd;

    [SerializeField]
    private bool repeatBeforeCompleteLevel;

    public bool RepeatBeforeCompleteLevel => repeatBeforeCompleteLevel;

    public static Action<TutorialItem> OnShow;
    public static Action<TutorialItem> OnHide;

    private bool activated;

    private bool targetChanged = false;

    public void Init()
    {
        WindowBase.OnOpen += OnOpen;
        WindowBase.OnClose += OnClose;
        GameManager.OnEndLevel += OnEndLevel;
        SubLevel.OnEndSubLevel -= OnEndSubLevel;
        SubLevel.OnEndSubLevel += OnEndSubLevel;
        SubLevel.OnTargetChanged += OnTargetChanged;
        if (!waitEvent)
            Tutorial.instance.StartCoroutine(CheckActivation());
    }

    private void OnEndSubLevel(SubLevel _subLevel, bool _success)
    {
        if (repeatBeforeCompleteLevel && !_success && Player.Instance.CompletedLevel < Level)
        {
            activated = false;
        }
    }

    private void OnEndLevel(Level _level)
    {
        Tutorial.instance.StartCoroutine(CheckActivation());
    }

    private void OnTargetChanged()
    {
        targetChanged = true;
        Tutorial.instance.StartCoroutine(CheckActivation());
    }

    private void OnClose(WindowBase _windowBase)
    {
        Tutorial.instance.StartCoroutine(CheckActivation());
    }

    private void OnOpen(WindowBase _windowBase)
    {
        Tutorial.instance.StartCoroutine(CheckActivation());
    }

    private IEnumerator CheckActivation()
    {
        yield return new WaitForSeconds(0.3f);
        if (target.gameObject.activeInHierarchy && !activated && Player.Instance.UnlockedLevel == Level)
        {
            if (waitTargetChanged && !targetChanged)
                yield return null;
            WindowBase windowBase = GUIManager.Instance.GetWindow(windowTypeName);
            if (windowBase == null || (tutorialWindowMode == TutorialWindowMode.OnOpen && windowBase.Opened) ||
                (tutorialWindowMode == TutorialWindowMode.OnClose && !windowBase.Opened))
            {
                activated = true;
                OnShow.Execute(this);
                Button button = target.GetComponent<Button>();
                if (button != null)
                {
                    button.onClick.RemoveListener(Hide);
                    button.onClick.AddListener(Hide);
                }

                if (time > 0)
                    Tutorial.instance.StartCoroutine(Hide(time));
            }
        }
    }

    public void Hide()
    {
        Tutorial.instance.StartCoroutine(Hide(0));
    }
    
    private IEnumerator Hide(int _time)
    {
        if (_time > 0)
            yield return new WaitForSeconds(_time);
        else yield return null;
        OnHide.Execute(this);
    }

    public void DeInit()
    {
        Button button = target.GetComponent<Button>();
        if (button != null)
        {
            button.onClick.RemoveListener(Hide);
        }

        WindowBase.OnOpen -= OnOpen;
        WindowBase.OnClose -= OnClose;
        GameManager.OnEndLevel -= OnEndLevel;
        SubLevel.OnTargetChanged -= OnTargetChanged;
    }
}

[Serializable]
public enum TutorialWindowMode
{
    OnOpen,
    OnClose
}
