using System;
using UnityEngine;

/// <summary>
/// Менеджер обучалки
/// </summary>
public class Tutorial : MonoBehaviour
{
    [SerializeField]
    private GameObject arrowPrefab;

    [SerializeField]
    private TutorialItem[] tutorialItems;

    private GameObject arrow;

    private TutorialItem nextItem;

    public static Tutorial instance;

    private void Awake()
    {
        instance = this;
        TutorialItem.OnShow += OnShow;
        TutorialItem.OnHide += OnHide;
        SubLevel.OnEndSubLevel += OnEndSubLevel;
    }

    private void OnEndSubLevel(SubLevel _subLevel, bool _success)
    {
        if (!_success)
        {
            foreach (TutorialItem tutorialItem in tutorialItems)
            {
                if (tutorialItem.RepeatBeforeCompleteLevel && Player.Instance.CompletedLevel < tutorialItem.Level)
                {
                    if (nextItem != null)
                        nextItem.DeInit();
                    tutorialItem.Init();
                    break;
                }
            }
        }
    }

    public void Init()
    {
        foreach (TutorialItem tutorialItem in tutorialItems)
        {
            if (tutorialItem.Level >= Player.Instance.CompletedLevel)
            {
                tutorialItem.Init();
                break;
            }
        }
    }

    private void OnShow(TutorialItem _tutorialItem)
    {
        arrow = Instantiate(arrowPrefab, _tutorialItem.Target);
        if (!string.IsNullOrEmpty(_tutorialItem.Text) && !_tutorialItem.ShowTextInTheEnd)
            (GUIManager.Instance.GetWindow<TextHint>() as TextHint).Show(_tutorialItem.Text);
    }

    private void OnHide(TutorialItem _tutorialItem)
    {
        if (!string.IsNullOrEmpty(_tutorialItem.Text) && _tutorialItem.ShowTextInTheEnd)
            (GUIManager.Instance.GetWindow<TextHint>() as TextHint).Show(_tutorialItem.Text);
        _tutorialItem.DeInit();
        
        DestroyImmediate(arrow);
        nextItem = Array.IndexOf(tutorialItems, _tutorialItem) < tutorialItems.Length - 1 ? tutorialItems[Array.IndexOf(tutorialItems, _tutorialItem) + 1] : null;
        if (nextItem != null)
            nextItem.Init();
    }

    private void OnDestroy()
    {
        TutorialItem.OnShow -= OnShow;
        TutorialItem.OnHide -= OnHide;
        SubLevel.OnEndSubLevel -= OnEndSubLevel;
    }
}
