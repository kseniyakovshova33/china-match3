﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Список префабов GUIItem с пулингом
/// </summary>
public class GUIList : MonoBehaviour, IEnumerable<GUIItem>
{
    #region Events

    /// <summary>
    /// Событие добавления префаба в список
    /// </summary>
    public event Action<GUIItem> OnAddItem;

    #endregion

    [SerializeField] private Transform prefab;

    //TODO: Сделать private
    public List<GUIItem> entries = new List<GUIItem>();

    [SerializeField] private Transform grid;

    public GUIItem this[int _index]
    {
        get { return entries.Get(_index); }
    }

    public int Count
    {
        get { return entries.FindAll(e => e.gameObject.activeSelf == true).Count; }
    }

    #region Mono

    private void Awake()
    {
        //Clear();
    }

    private void OnDestroy()
    {
        Clear();
    }

#if UNITY_EDITOR

    void OnApplicationQuit()
    {
        Clear();
    }

    private void Reset()
    {
        var found = GetComponentInChildren<GUIItem>();
        if (found != null)
        {
            prefab = found.transform;
            grid = prefab.parent;
        }
        else
            grid = transform;

        OnValidate();
    }

    private void OnValidate()
    {
        if (Application.isPlaying || grid == null)
            return;
        entries.Clear();
        foreach (Transform elem in grid)
        {
            var pre = elem.GetComponent<GUIItem>();
            if (pre != null)
                entries.Add(pre);
        }
    }

    public GameObject GetPrefab()
    {
        return prefab == null ? null : prefab.gameObject;
    }
#endif

    #endregion

    #region GetItem

    /// <summary>
    /// Создаение копии префаба
    /// </summary>
    private GUIItem AddItemObject()
    {
        //Так делать не надо - ломаются некоторые списки (причины не ясны, возможно баг юнити)
        //Transform newItemObjectTransform = Instantiate(prefab, grid != null ? grid : transform, false);

        Transform newItemObjectTransform = Instantiate(prefab);
        newItemObjectTransform.SetParent(grid ? grid : transform, false);
        newItemObjectTransform.localScale = Vector3.one;
        newItemObjectTransform.localPosition = Vector3.zero;
#if UNITY_EDITOR
        newItemObjectTransform.name = string.Format("{0} {1}", prefab.name, entries.Count);
#endif
        GUIItem newItem = newItemObjectTransform.GetComponent<GUIItem>();
        newItem.gameObject.SetActive(true);
        entries.Add(newItem);
        OnAddItem.Execute(newItem);
        return newItem;
    }

    [Obsolete("Use GetItem instead")]
    public GUIItem getElement(int _index)
    {
        return GetItem(_index);
    }

    /// <summary>
    /// Получить активный элемент с порядковым номером.
    /// </summary>
    public GUIItem GetItem(int _index, bool _createIfNotExists = true)
    {
        GUIItem elem;
        if (entries.Count > _index)
        {
            elem = entries[_index];
            elem.gameObject.SetActive(true);
        }
        else
            elem = _createIfNotExists ? AddItemObject() : null;

        if (elem != null)
            elem.index = _index;
        return elem;
    }

    /// <summary>
    /// Получить активный элемент с порядковым номером.
    /// </summary>
    public T GetItem<T>(int _index, bool _createIfNotExists = true)
        where T : GUIItem
    {
        return (T) GetItem(_index, _createIfNotExists);
    }

    public GUIItem GetItem(Predicate<GUIItem> _customCondition = null, bool _createIfNotExists = true)
    {
        var item = _customCondition == null
            ? entries.Find(e => !e.gameObject.activeSelf)
            : entries.Find(_customCondition);
        
        if (item == null)
        {
            if (_createIfNotExists)
                item = AddItemObject();
        }
        else
        {
            //item.transform.SetSiblingIndex(item.transform.parent.childCount - 1);
            item.gameObject.SetActive(true);
        }

        if (item != null)
            item.index = 0;

        return item;
    }

    public T GetItem<T>(Predicate<GUIItem> _customCondition = null, bool _createIfNotExists = true)
        where T : GUIItem
    {
        return (T) GetItem(_customCondition, _createIfNotExists);
    }

    #endregion

    #region Clear

    /// <summary>
    /// Скрывает все существующие элементы
    /// </summary>
    public void Clear()
    {
        Clear(0);
    }

    /// <summary>
    /// Скрывает все элементы в списке кроме указанного количества
    /// </summary>
    /// <param name="_amount">Количество элементов, которые должны остаться видимы</param>
    /// <param name="_addLacking">Создавать новые элементы если число уже созданных элементов меньше запрошеного</param>
    public void Clear(int _amount, bool _addLacking = false)
    {
        if (entries.Count < _amount)
            _amount = entries.Count;

        for (int i = 0; i < _amount; i++)
            entries[i].gameObject.SetActive(true);

        for (int i = _amount; i < entries.Count; i++)
        {
            var entry = entries[i];
            if (entry != null)
            {
                //TODO: Переносить в отдельный список, а не просто отключать
                entry.Deinit();
                entry.gameObject.SetActive(false);
            }
        }

        if (_addLacking)
            if (entries.Count < _amount)
            {
                int objectsToCreate = _amount - entries.Count;
                for (int i = 0; i < objectsToCreate; i++)
                    AddItemObject();
            }
    }

    #endregion

    #region IEnumerator

    public IEnumerator<GUIItem> GetEnumerator()
    {
        if (entries.Count == 0)
            yield break;
        for (int i = 0; i < entries.Count; i++)
        {
            if (entries[i].gameObject.activeSelf || entries[i].IsInitialized)
                yield return entries[i];
        }

        yield break;
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    #endregion
}