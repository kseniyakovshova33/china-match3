﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class GUIItem : MonoBehaviour
{
    #region Events

    /// <summary>
    /// Событие Инициализации
    /// </summary>
    public event Action<GUIItem> OnInit;

    /// <summary>
    /// Событие Деинициализации
    /// </summary>
    public event Action<GUIItem> OnDeInit;

    #endregion

    [HideInInspector] public int index = 0;
#if USING_SOUNDS
    [SerializeField, Sound] protected string SoundOnAppear = "";
#endif

    private Shadow shadow;
    private Outline outline;
    private Text effectsForText;

    private void GetEffects(Text _text)
    {
        if (effectsForText == _text || _text == null)
            return;
        outline = _text.GetComponent<Outline>();
        shadow = _text.GetComponent<Shadow>();
        effectsForText = _text;
    }

    /// <summary>
    /// Инициализирован ли объект. Должно контроллироваться через функции Init/Deinit
    /// </summary>
    public bool IsInitialized { get; protected set; }

    /// <summary>
    /// Инициализирует объект - меняет флаг IsInitialized
    /// </summary>
    public virtual void Init()
    {
#if USING_SOUNDS
        if (!IsInitialized && !string.IsNullOrEmpty(SoundOnAppear))
            AudioController.Play(SoundOnAppear);
#endif
        IsInitialized = true;
        OnInit.Execute(this);
    }

    /// <summary>
    /// Деинициализирует объект - меняет флаг IsInitialized
    /// </summary>
    public virtual void Deinit()
    {
        IsInitialized = false;
        OnDeInit.Execute(this);
    }
}