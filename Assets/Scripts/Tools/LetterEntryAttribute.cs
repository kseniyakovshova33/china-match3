using System; 
using System.Collections; 
using UnityEditor; 
using UnityEngine;
#if UNITY_EDITOR 
using Sirenix.OdinInspector.Editor;  
#endif

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
public class LetterEntryAttribute :  PropertyAttribute { }

#if UNITY_EDITOR
public class LetterAttributeOdinDrawer<TAttribute> : OdinAttributeDrawer<TAttribute>, IDefinesGenericMenuItems
    where TAttribute : LetterEntryAttribute
{
    public override bool CanDrawTypeFilter(Type type)
    {
        return !typeof(ICollection).IsAssignableFrom(type);
    }

    private LettersEditorUtilities.Drawer drawer;

    protected override void Initialize()
    {
        base.Initialize();
        drawer = new LettersEditorUtilities.Drawer(typeof(Letter), Property);
    }

    protected sealed override void DrawPropertyLayout(GUIContent _label)
    {
        if (!drawer.DrawPropertyLayout(_label)) this.CallNextDrawer(_label);
    }

    public void PopulateGenericMenu(InspectorProperty _property, GenericMenu _genericMenu)
    {
        drawer.PopulateGenericMenu(_property, _genericMenu);
    }
}
#endif