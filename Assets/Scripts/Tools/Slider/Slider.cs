using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

/// <summary>
/// Базовый класс слайдера. Управляет уровнем заполненности. Унаследованные классы анимируют изменения значения в сцене
/// </summary>
public abstract class Slider : MonoBehaviour 
{
    private float value;
    
    [ShowInInspector][DisableInPlayMode]
    public virtual float Value
    {
        set { this.value = Mathf.Clamp01(value);}
        get { return value;}
    }

    private Coroutine routine;
    public void SetAnimated(float _value, float _time, Easing.EaseFunction _easeFunction = null, bool _useFixedUpdate = false )
    {
        if (routine != null)
            StopCoroutine(routine);
        routine = this.StartCoroutineSafe(AnimEnum(_value, _time, _easeFunction ?? Easing.linear, _useFixedUpdate));
        if (routine == null)
            Value = _value;
    }

    private IEnumerator AnimEnum(float _value, float _time, Easing.EaseFunction _easeFunction, bool _useFixedUpdate = false)
    {
        var waiter = _useFixedUpdate ? new WaitForFixedUpdate() : null;
        float t = 0f;
        float step = 1f / _time;
        float startValue = value;
        while (t < 1f)
        {
            Value = _easeFunction(startValue, _value, t);
            t += (_useFixedUpdate? Time.fixedDeltaTime: Time.deltaTime) * step;
            yield return waiter;
        }

        Value = _value;
        routine = null;
        yield break;
    }
}