using System;
using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;

public class SizeSlider : Slider
    {
        public RectTransform Image;
        public bool InvertSlider = false;

        [SerializeField, Header("Safety")] private bool useSafeWidth = true;
        [SerializeField, ShowIf("useSafeWidth")] private float safeWidthPart = 0.1f;

        private bool isInitialized = false;

        private float width;
        private float height;

        private bool isStretchedX = false;
        private bool isStretchedY = false;


        private void Init()
        {
            width = Image.rect.width;
            height = Image.rect.height;

            isStretchedX = Image.anchorMin.x != Image.anchorMax.x;
            isStretchedY = Image.anchorMin.y != Image.anchorMax.y;

            if (width > 0f && height > 0f)
            {
                isInitialized = true;
                Value = Value;
            }
            else
            {
                if (initTimeoutRoutine == null)
                    initTimeoutRoutine = StartCoroutine(InitTimeout());
            }
        }

        protected virtual void OnDisable()
        {
            this.StopCoroutine(ref initTimeoutRoutine);
        }

        private Coroutine initTimeoutRoutine;

        private IEnumerator InitTimeout()
        {
            yield return null;
            initTimeoutRoutine = null;
            Init();
        }

        public override float Value
        {
            set
            {
                base.Value = value;
                if (!isInitialized)
                    Init();
                float val = InvertSlider ? 1f - base.Value : base.Value;
                if(useSafeWidth)
                    Image.sizeDelta = new Vector2(isStretchedX ? Math.Max(width * (val - 1f), -width * (1f - safeWidthPart)) : Math.Max(width * val, width * safeWidthPart), isStretchedY ? 0f : height);
                else
                    Image.sizeDelta = new Vector2(isStretchedX ? width * (val - 1f) : width * val, isStretchedY ? 0f : height);
            }
        }

        private void Reset()
        {
            Image = transform as RectTransform;
        }
    }