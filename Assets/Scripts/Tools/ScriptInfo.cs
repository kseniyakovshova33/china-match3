using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class ScriptInfo
{
    public Type BaseClass;
    public List<Type> Subtypes;
    public GUIContent[] Names;
    public GUIContent[] NamesFlat;

    public Type this[int _i]
    {
        get
        {
            if (_i < 0 || _i >= Subtypes.Count)
                return null;
            return Subtypes[_i];
        }
    }

    private string GetFullPath(Type _type, Type _baseClass, string _previousPath = "")
    {
        if (_type.BaseType == null || _type.BaseType == typeof(System.Object) || _type.BaseType == _baseClass || _type == _baseClass)
            return _previousPath != String.Empty ? _type.Name + "/" + _previousPath : _type.Name;
        return GetFullPath(_type.BaseType, _baseClass, _previousPath != String.Empty ? _type.Name + "/" + _previousPath : _type.Name);
    }

    private static WeakReference<List<Type>> cachedTypes = new WeakReference<List<Type>>(null);

    /// <summary>
    /// Список всех типов во всех сборках проекта. Периодически уничтожается сборщиком мусора
    /// </summary>
    public static List<Type> AllTypes
    {
        get
        {
            List<Type> allTypes;
            if (!cachedTypes.TryGetTarget(out allTypes))
            {
                cachedTypes.SetTarget(allTypes = new List<Type>());
                Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
                if (assemblies != null)
                {
                    for (int i = 0; i < assemblies.Length; i++)
                    {
                        if (assemblies[i] != null)
                        {
                            try
                            {
                                Type[] types = assemblies[i].GetTypes();
                                for (int j = 0; j < types.Length; j++)
                                {
                                    if (types[j] != null)
                                    {
                                        allTypes.Add(types[j]);
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                Debug.LogWarning("Cant load assembly for " + assemblies[i].ToString()+" with error "+e.Message);
                            }
                        }
                    }
                }
            }

            return allTypes;
        }
    }

    private ScriptInfo(Type _baseClass, bool _includeBase, bool _includeNull, bool _includeAbstract)
    {
        BaseClass = _baseClass;
        Subtypes = new List<Type>();

#if UNITY_2019_2_OR_NEWER && UNITY_EDITOR
        Subtypes.AddRange( UnityEditor.TypeCache.GetTypesDerivedFrom(_baseClass) );
#else
        foreach (Type type in AllTypes)
        {
            if (_baseClass.IsAssignableFrom(type) && !type.ContainsGenericParameters && !type.IsAbstract)
                Subtypes.Add(type);
        }
#endif

        //Subtypes = ( from Type type in types where ( _baseClass.IsAssignableFrom( type ) && !type.ContainsGenericParameters && !type.IsAbstract ) select type ).ToList();
        if (_includeNull)
            Subtypes.Insert(0, null);
        if (!_includeBase)
            Subtypes.Remove(_baseClass);
        else if (!Subtypes.Contains(_baseClass)) 
            Subtypes.Insert(_includeNull ? 1 : 0, _baseClass);
        if (!_includeAbstract)
            Subtypes.RemoveAll(t => t != null && t.IsAbstract);

        Subtypes.Sort((_type, _type1) =>
        {
            if (_type == null)
                return -1;
            if (_type1 == null)
                return 1;
            return string.Compare(GetFullPath(_type, _baseClass), GetFullPath(_type1, _baseClass));
        });

        Names = new GUIContent[Subtypes.Count];
        NamesFlat = new GUIContent[Subtypes.Count];
        for (int i = Subtypes.Count - 1; i >= 0; i--)
        {
            if (Subtypes[i] != null)
            {
                string suffix = Subtypes.FindAll(t => t != null && t.Name == Subtypes[i].Name).Count > 1 
                        ? $" (from {Subtypes[i].Assembly.FullName})"
                        : string.Empty;
                    
                Names[i] = new GUIContent(GetFullPath(Subtypes[i], _baseClass) + suffix);
                NamesFlat[i] = new GUIContent(Subtypes[i].Name + suffix);
            }
            else
            {
                Names[i] = new GUIContent("noone");
                NamesFlat[i] = new GUIContent("noone");
            }
        }
    }

    public int FindIndex(Type _targetClass)
    {
        return Subtypes.IndexOf(_targetClass);
    }

    private struct Filter
    {
        public Type Type;
        public bool IncludeBase;
        public bool IncludeNull;
        public bool IncludeAbstract;

        public Filter(Type _type, bool _includeBase, bool _includeNull, bool _includeAbstract)
        {
            Type = _type;
            IncludeBase = _includeBase;
            IncludeNull = _includeNull;
            IncludeAbstract = _includeAbstract;
        }
    }

    private static Dictionary<Filter, ScriptInfo> scriptSearchIndexer = new Dictionary<Filter, ScriptInfo>();


    /// <summary>
    /// Возвращает список всех Не-Generic классов, происходищих от _baseClass
    /// Созданные функцией данные хранятся внутри класса для быстрого повторного доступа
    /// </summary>
    /// <param name="_baseClass">Базовый класс</param>
    /// <param name="_includeBase">Включать ли базовый класс в реультаты</param>
    /// <param name="_includeNull">Включает null в начало списка</param>
    public static ScriptInfo Get(Type _baseClass, bool _includeBase, bool _includeNull, bool _includeAbstract = true)
    {
        ScriptInfo result;
        Filter filter = new Filter(_baseClass, _includeBase, _includeNull, _includeAbstract);
        if (!scriptSearchIndexer.TryGetValue(filter, out result))
        {
            result = GetOnce(_baseClass, _includeBase, _includeNull, _includeAbstract);
            scriptSearchIndexer.Add(filter, result);
        }

        return result;
    }

    /// <summary>
    /// Возвращает список всех Не-Generic классов, происходищих от _baseClass
    /// Созданные функцией данные хранятся внутри класса для быстрого повторного доступа
    /// </summary>
    /// <param name="_baseClass">Базовый класс</param>
    /// <param name="_includeBase">Включать ли базовый класс в реультаты</param>
    /// <param name="_includeNull">Включает null в начало списка</param>
    public static ScriptInfo GetOnce(Type _baseClass, bool _includeBase, bool _includeNull, bool _includeAbstract = true)
    {
        return new ScriptInfo(_baseClass, _includeBase, _includeNull, _includeAbstract);
    }
}