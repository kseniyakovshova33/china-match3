using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public static class Extensions
{
    public static T GetRandom<T>(this IList<T> _list)
    {
        return _list[Random.Range(0, _list.Count)];
    }

    public static void AddOnce<T>(this IList<T> _list, T _element)
    {
        if (!_list.Contains(_element))
            _list.Add(_element);
    }
    
    /// <summary>
    /// Вызывает делегат с предварительной проверкой на null
    /// </summary>
    public static void Execute(this Action _action)
    {
        _action?.Invoke();
    }
    
    public static void Execute<T>(this Action<T> _action, T _a1)
    {
        _action?.Invoke(_a1);
    }
    
    public static T Get<T>(this IList<T> _list, int _index, T _def = default(T))
    {
        if (_list == null || (uint)_index >= (uint)_list.Count)
            return _def;
        return _list[_index];
    }
}
