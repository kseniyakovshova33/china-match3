﻿using UnityEngine;

public static class ExGeneric
{
    /// <summary>
    /// Приблизительное сравнения чисел 
    /// </summary>
    public static bool IsEqual(this float a, float b, float delta = 1E-10f)
    {
        return Mathf.Abs((float) (b - a)) <= delta;
    }

    /// <summary>
    /// Приблизительное сравнения чисел 
    /// </summary>
    public static bool IsEqual(this double a, double b, double delta = 1E-10)
    {
        return Mathf.Abs((float) (b - a)) <= delta;
    }
}