using System;
using System.Collections;
using UnityEngine;

public static class ExCoroutine
{
    /// <summary>
    /// Останавливает корутину если она существует и присваивает значению null
    /// </summary>
    /// <returns>Истина если корутина работала и была остановлена</returns>
    public static bool StopCoroutine(this MonoBehaviour _behaviour, ref Coroutine _routine)
    {
        if (_routine == null)
            return false;
        _behaviour.StopCoroutine(_routine);
        _routine = null;
        return true;
    }

    public static void StartCoroutine(this MonoBehaviour _behaviour, IEnumerator _enum, ref Coroutine _routine)
    {
        if (_routine != null)
            _behaviour.StopCoroutine(_routine);
        _routine = _behaviour.StartCoroutine(_enum);
    }

    public class Coroutine<T>
    {
        public Coroutine Task;
        public T Result;

        public static implicit operator Coroutine(Coroutine<T> _wrapper)
        {
            return _wrapper?.Task;
        }
    }

    public static Coroutine<T> StartCoroutine<T>(this MonoBehaviour _behaviour, IEnumerator _enum)
    {
        Coroutine<T> result = new Coroutine<T>();

        IEnumerator Routine()
        {
            while (_enum.MoveNext())
            {
                object current = _enum.Current;
                if (current is T r)
                    result.Result = r;
                else
                    yield return current;
            }
        }

        result.Task = _behaviour.StartCoroutine(Routine());

        return result;
    }

    /// <summary>
    /// Вызывает указанную функцию через указанное время в секундах.
    /// Функция использует корутину, так что объект _owner должен быть активен.
    /// </summary>
    /// <returns></returns>
    public static Coroutine CallTimeout(this MonoBehaviour _owner, Action _method, float _time = 0f)
    {
        return _owner.StartCoroutineSafe(Caller(_method, _time));
    }

    private static IEnumerator Caller(Action _call, float _timer)
    {
        if (_timer > 0f)
            yield return new WaitForSeconds(_timer);
        else
            yield return null;
        _call.Execute();
    }

    public static Coroutine StartCoroutineSafe(this MonoBehaviour _component, IEnumerator _routine)
    {
        if (_component == null || !_component.isActiveAndEnabled)
            return null;
        return _component.StartCoroutine(_routine);
    }
}
