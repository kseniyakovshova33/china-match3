﻿using System;
using UnityEngine;

/// <summary>
/// Перемещает точки привязки элемента интерфейса.<br/>
/// Позволяет плавно наводить камеру на части интерфейсных объектов, которые не помещаются в экран целиком.
/// </summary>
public class TweenSize : UITweener
{
    public Vector2 from;
    public Vector2 to;

    private RectTransform mTrans;

    private RectTransform rectTransform
    {
        get
        {
            if (mTrans == null)
            {
                mTrans = GetComponent<RectTransform>();
#if UNITY_EDITOR || DEBUG
                if (mTrans == null)
                {
                    Debug.LogError("RectTransform is null in TweenAnchor! Plz Fix it at " + name + "!");
                }
#endif
            }
            return mTrans;
        }
    }

    public Vector2 value
    {
        get
        {
            return rectTransform == null ? Vector2.zero : rectTransform.sizeDelta;
        }
        set
        {
            if (rectTransform != null)
                rectTransform.sizeDelta = value;
        }
    }

    protected override void OnUpdate(float factor, bool isFinished) => value = Vector2.Lerp(from, to, Easing.easeInOutSine(0f, 1f, factor));

    public override void SetStartToCurrentValue()
    {
        from = value;
    }

    public override void SetEndToCurrentValue()
    {
        to = value;
    }
}