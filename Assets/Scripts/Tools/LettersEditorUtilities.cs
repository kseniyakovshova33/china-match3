#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using Sirenix.Utilities.Editor;
using UnityEditor;
using UnityEngine;
using Sirenix.OdinInspector.Editor;

public static class LettersEditorUtilities
{
    #region Assets

    private static LettersDictionary existingDictionary;

    private static LettersDictionary GetLettersDictionary()
    {
        if (existingDictionary != null) return existingDictionary;

        string[] found = AssetDatabase.FindAssets("t:LettersDictionary");

        var path = AssetDatabase.GUIDToAssetPath(found[0]);
        existingDictionary = AssetDatabase.LoadAssetAtPath<LettersDictionary>(path);
        return existingDictionary;
    }

    #endregion

    #region Entry Drawer
    
    /// <summary>
    /// Рисует поле выбора элемента БД
    /// </summary>
    public static Letter DrawEntry(GUIContent _label, Letter _entry, Type _entryType, GUIStyle _style = null,
        params GUILayoutOption[] _options)
    {
        SirenixEditorGUI.GetFeatureRichControlRect(_label, out _, out _, out var valueRect, _options);
        return DrawEntry(valueRect, null, _entry, _entryType, _style);
    }

    /// <summary>
    /// Рисует поле выбора элемента БД
    /// </summary>
    public static Letter DrawEntry(Rect _rect, GUIContent _label, Letter _entry, Type _entryType,
        GUIStyle _style = null)
    {
        return DrawEntry(_rect, _label, _entry, GetLettersDictionary(), null, _style);
    }

    private static Letter DrawEntry(Rect _rect, GUIContent _label, Letter _entry, LettersDictionary _owner,
        Func<Letter, bool> _filter, GUIStyle _style = null)
    {
        GUIContent content = _entry != null
            ? new GUIContent(_entry.Translation, _entry.Icon != null ? AssetPreview.GetAssetPreview(_entry.Icon) : null)
            : new GUIContent("None");

        OdinSelector<Letter> GetSelector(Rect _r)
        {
            void AddToList(List<ValueDropdownItem<Letter>> _list, LettersDictionary _db)
            {
                foreach (Letter entry in _db)
                {
                    if (_filter == null || _filter(_entry))
                    {
                        _list.Add(new ValueDropdownItem<Letter>(" [ #" + entry.Id + " ]" + entry.Translation, entry));
                    }
                }
            }

            List<ValueDropdownItem<Letter>> lst = new List<ValueDropdownItem<Letter>>();
            lst.Add(new ValueDropdownItem<Letter>("None", null));

            AddToList(lst, _owner);

            OdinSelector<Letter> selector = ShowSelector(_r, Vector2.zero, lst, _entry);
            selector.SelectionTree.EnumerateTree().AddIcons(_t => (_t.Value as Letter)?.Icon);
            return selector;
        }

        if (_label != null && !string.IsNullOrEmpty(_label.text))
            _rect = EditorGUI.PrefixLabel(_rect, _label);

        GUIHelper.PushIndentLevel(0);

        IEnumerable<Letter> result =
            OdinSelector<Letter>.DrawSelectorDropdown(_rect, content, GetSelector, _style);

        GUIHelper.PopIndentLevel();

        if (result != null)
        {
            using (var enumer = result.GetEnumerator())
                return enumer.MoveNext() ? enumer.Current : null;
        }


        return _entry;
    }

    private static OdinSelector<T> ShowSelector<T>(Rect _rect, Vector2 _dropdownSize,
        IEnumerable<ValueDropdownItem<T>> _values, T _selection, int _minItemsForSerach = 10,
        string _dropdownTitle = null, bool _addIcons = true)
    {
        bool flag = _minItemsForSerach == 0 || _values != null &&
                    _values.Take(_minItemsForSerach).Count() ==
                    _minItemsForSerach;
        GenericSelector<T> selector = new GenericSelector<T>(_dropdownTitle, false,
            _values.Select(
                x =>
                    new GenericSelectorItem<T>(x.Text, x.Value)));

        selector.SelectionTree.Config.DrawSearchToolbar = flag;
        selector.SetSelection(_selection);
        if (_addIcons)
            selector.SelectionTree.EnumerateTree().AddThumbnailIcons(true);
        selector.FlattenedTree = false;

        _rect.x = (int) _rect.x;
        _rect.y = (int) _rect.y;
        _rect.width = (int) _rect.width;
        _rect.height = (int) _rect.height;
        _rect.xMax = GUIHelper.GetCurrentLayoutRect().xMax;
        selector.ShowInPopup(_rect, _dropdownSize);
        return selector;
    }

    #endregion

    #region Free Type Drawer

    public class Drawer : IDefinesGenericMenuItems
    {
        protected Type entryType;
        protected LettersDictionary db;
        protected InspectorProperty Property;

        private enum Mode
        {
            Int,
            String
        }

        private Mode mode;
        private string errorMessage;

        public Drawer(Type dataType, InspectorProperty property)
        {
            Property = property;
            errorMessage = null;
            db = GetLettersDictionary();

            if (db == null)
                errorMessage = "Database of type '" + dataType + "' not found";

            entryType = dataType;

            if (entryType == null)
                errorMessage = "Type must be derived from Database or DatabaseEntry: " + dataType;

            if (errorMessage != null)
                return;

            var valueType = property.ValueEntry.TypeOfValue;
            if (valueType == typeof(int))
            {
                mode = Mode.Int;
                return;
            }

            if (valueType == typeof(string))
            {
                mode = Mode.String;
                return;
            }

            errorMessage = "This value type is not supported for DatbaseEntryAttribute: " + valueType.Name;
        }

        public bool DrawPropertyLayout(GUIContent _label)
        {
            if (errorMessage != null)
            {
                SirenixEditorGUI.ErrorMessageBox(errorMessage);
                return false;
            }

            switch (mode)
            {
                case Mode.Int:
                    DrawPropetyNumber(_label);
                    break;

                case Mode.String:
                    DrawPropetyString(_label);
                    break;
            }

            return true;
        }

        protected void DrawPropetyNumber(GUIContent _label)
        {
            int value = (int) Property.ValueEntry.WeakSmartValue;
            Letter entry = DrawEntry(_label, db[value], entryType);
            Property.ValueEntry.WeakSmartValue = entry != null ? entry.Id : -1;
        }

        protected void DrawPropetyString(GUIContent _label)
        {
            string value = (string) Property.ValueEntry.WeakSmartValue;
            Letter entry = DrawEntry(_label, db[value], entryType);
            Property.ValueEntry.WeakSmartValue = entry != null ? entry.Translation : string.Empty;
        }

        public void PopulateGenericMenu(InspectorProperty _property, GenericMenu _genericMenu)
        {
            if (errorMessage != null)
                return;
            _genericMenu.AddItem(new GUIContent("Clear"), false, () =>
            {
                switch (mode)
                {
                    case Mode.Int:
                        _property.ValueEntry.WeakSmartValue = -1;
                        break;

                    case Mode.String:
                        _property.ValueEntry.WeakSmartValue = string.Empty;
                        break;
                }
            });
        }
    }

    #endregion
}
#endif
