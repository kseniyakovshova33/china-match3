using UnityEngine;
using UnityEngine.UI;

public class PlaySoundOnClick : MonoBehaviour
{
    [SerializeField]
    private AudioClip audioClip;

    private Button button;
    
    private void Awake()
    {
        button = GetComponent<Button>();
    }

    private void OnEnable()
    {
        if (button != null)
            button.onClick.AddListener(PlaySound);
    }

    private void PlaySound()
    {
        SoundManager.Instance.PlaySound(audioClip);
    }
    
    private void OnDisable()
    {
        if (button != null)
            button.onClick.RemoveListener(PlaySound);
    }
}
