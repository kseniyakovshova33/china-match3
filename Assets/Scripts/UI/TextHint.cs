using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Текстовый хинт поверх игры
/// </summary>
public class TextHint : WindowBase
{
    [SerializeField]
    private Text text;

    private void Awake()
    {
        TextData.OnShown += OnShown;
    }

    private void OnDestroy()
    {
        TextData.OnShown -= OnShown;
    }

    public void Show(string _text)
    {
        OnShown(_text);
    }
    
    public void ShowImmideatly(string _text)
    {
        text.text = _text;
        Open(false);
    }

    private void OnShown(string _text)
    {
        text.text = _text;
        GUIManager.Instance.StartCoroutine(ShowWithDelay());
    }

    private IEnumerator ShowWithDelay()
    {
        yield return new WaitForSeconds(0.7f);
        Open(false);
    }
}
