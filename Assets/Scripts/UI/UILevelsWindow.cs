using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Окно уровней
/// </summary>
public class UILevelsWindow : WindowBase
{
    [SerializeField]
    private Button dictionaryButton;
    
    [SerializeField]
    private Button settingsButton;
    
    [SerializeField]
    private GUIList levels;

    [SerializeField]
    private Text priceText;

    [SerializeField]
    private ScrollRect scrollRect;

    private void Awake()
    {
        foreach (Level level in LevelManager.Instance)
        {
            UILevel uiLevel = levels.GetItem<UILevel>();
            uiLevel.Init(level);
        }
        GameManager.OnStartLevel += OnStartLevel;
        GameManager.OnEndLevel += OnEndLevel;
        dictionaryButton.onClick.AddListener(() => GUIManager.Instance.GetWindow<UIDictionary>().Open(_prevWindow: this));
        settingsButton.onClick.AddListener(() => GUIManager.Instance.GetWindow<UISettings>().Open(_prevWindow: this));
    }

    private void OnEnable()
    {
        RefreshText();
        Canvas.ForceUpdateCanvases();
        GUIItem targetItem = levels.GetItem<UILevel>(x => (x as UILevel).Level.Number == Player.Instance.UnlockedLevel);
        if (Player.Instance.UnlockedLevel < levels.Count)
            scrollRect.content.localPosition = new Vector3(scrollRect.content.localPosition.x, -targetItem.transform.localPosition.y - 1000);
    }

    private void OnDestroy()
    {
        GameManager.OnStartLevel -= OnStartLevel;
        GameManager.OnEndLevel -= OnEndLevel;
        dictionaryButton.onClick.RemoveAllListeners();
    }

    private void OnEndLevel(Level _level)
    {
        Open();
    }

    private void OnStartLevel(Level _level)
    {
        Close();
    }

    private void RefreshText()
    {
        priceText.text = Player.Instance.Score + " ¥";
    }
}
