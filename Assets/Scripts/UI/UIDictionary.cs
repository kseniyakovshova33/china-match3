using System.IO;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Окно словаря
/// </summary>
public class UIDictionary : WindowBase
{
    [SerializeField]
    private GUIList rows;

    [SerializeField]
    private Button closeButton;

    [SerializeField]
    private Button cardsButton;
    
    [SerializeField]
    private Button bookButton;
    
    [SerializeField]
    private Button courseButton;

    private void OnEnable()
    {
        foreach (Letter letter in LettersDictionary.Instance)
        {
            if (letter.IsUnlocked())
            {
                DictionaryRow dictionaryRow = rows.GetItem<DictionaryRow>();
                dictionaryRow.Init(letter);
            }
        }
        
        courseButton.onClick.AddListener(GetCourse);
        closeButton.onClick.AddListener(Close);
        cardsButton.onClick.AddListener(OpenCardsPdf);
        bookButton.onClick.AddListener(OpenBookPdf);
    }

    private void OpenCardsPdf()
    {
        string fileName = "https://disk.yandex.ru/i/o-NXK0StTx3uMg";
        Application.OpenURL(fileName);
    }

    private void OpenBookPdf()
    {
        string fileName = "https://disk.yandex.ru/i/R3kCkU_nqfo7pg";
        Application.OpenURL(fileName);
    }

    private void GetCourse()
    {
        Application.OpenURL("https://www.lingvo24.ru/");
    }

    private void OnDisable()
    {
        rows.Clear();
        closeButton.onClick.RemoveAllListeners();
        cardsButton.onClick.RemoveAllListeners();
        bookButton.onClick.RemoveAllListeners();
        courseButton.onClick.RemoveAllListeners();
    }
}
