using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

public class MainUI : WindowBase
{
    /// <summary>
    /// Название уровня
    /// </summary>
    [SerializeField]
    private Text levelName;
    
    /// <summary>
    /// Спрайт целевого иероглифа
    /// </summary>
    [SerializeField]
    private Image targetSprite;

    /// <summary>
    /// Перевод целевого иероглифа
    /// </summary>
    [SerializeField]
    private Text targetText;
    
    /// <summary>
    /// аниматор целевого иероглифа
    /// </summary>
    [SerializeField]
    private Animator targetAnimator;

    [SerializeField]
    private SizeSlider letterSlider;

    [SerializeField]
    private GUIList sliders;
    
    [SerializeField]
    private Button showButton;

    [SerializeField]
    private Text showPriceText;

    [SerializeField]
    private int showPrice;

    [SerializeField]
    private Button closeButton;

    [SerializeField]
    private Text priceText;
    
    [SerializeField]
    private Button dictionaryButton;

    [SerializeField] 
    private Button settingsButton;

    [SerializeField]
    private AudioClip wrongMovesSound, matchSound, changeTargetSount, showLetter;
    
    private void Awake()
    {
        SubLevel.OnStartSubLevel += OnStartSubLevel;
        SubLevel.OnTargetChanged += OnTargetChanged;
        GameManager.OnStartLevel += OnStartLevel;
        GameManager.OnEndLevel += OnEndLevel;
        BallFieldView.OnInit += OnInit;
        BallField.OnMatch += OnMatch;
        BallFieldView.OnWrongMove += OnWrongMoves;
        
        dictionaryButton.onClick.AddListener(() => GUIManager.Instance.GetWindow<UIDictionary>().Open(_prevWindow: this));
        settingsButton.onClick.AddListener(() => GUIManager.Instance.GetWindow<UISettings>().Open(_prevWindow: this));
        closeButton.onClick.AddListener(ReturnToMap);
        showButton.onClick.AddListener(ShowLetter);
    }

    private void OnDestroy()
    {
        SubLevel.OnStartSubLevel -= OnStartSubLevel;
        SubLevel.OnTargetChanged -= OnTargetChanged;
        BallFieldView.OnInit -= OnInit;
        BallField.OnMatch -= OnMatch;
        BallFieldView.OnWrongMove -= OnWrongMoves;
        GameManager.OnStartLevel -= OnStartLevel;
        GameManager.OnEndLevel -= OnEndLevel;
        dictionaryButton.onClick.RemoveAllListeners();
        settingsButton.onClick.RemoveAllListeners();
        closeButton.onClick.RemoveAllListeners();
        showButton.onClick.RemoveAllListeners();
    }
    
    private void OnWrongMoves()
    {
        SoundManager.Instance.PlaySound(wrongMovesSound);
    }

    private void ShowLetter()
    {
        if (Player.Instance.Score >= showPrice)
        {
            Player.Instance.Score -= showPrice;
            RefreshScore();
            showButton.gameObject.SetActive(false);
            targetSprite.enabled = true;
            SoundManager.Instance.PlaySound(showLetter);
        }
    }

    private void ReturnToMap()
    {
        GameManager.Instance.CurrentSubLevel.End(false);
        Close();
        GUIManager.Instance.GetWindow<UILevelsWindow>().Open();
    }
    
    private void OnInit()
    {
        RefreshTarget();
        showPriceText.text = showPrice + " ¥";
    }
    
    public void OnStartSubLevel(SubLevel _subLevel)
    {
        RefreshTarget();
        RefreshSliders(GameManager.Instance.CurrentLevel);
    }

    private void OnMatch(List<Ball> _match)
    {
        RefreshCurrentSlider();
        SubLevel subLevel = GameManager.Instance.CurrentSubLevel;
        letterSlider.SetAnimated((float) subLevel.CurrentLetterProgress / subLevel.NeedMatch, 0.3f);
        
        Letter target = LettersDictionary.Instance[GameManager.Instance.CurrentSubLevel.Target.Index];

        if (_match.Count > 0)
        {
            Player.Instance.Score += _match.Find(x => x.Letter == target) != null
                ? GameManager.Instance.CurrentSubLevel.TargetMatchPrice
                : GameManager.Instance.CurrentSubLevel.MatchPrice;
            RefreshScore();
            SoundManager.Instance.PlaySound(_match.Find(x => x.Letter == target) != null ? target.Pronunciation : matchSound);
        } 
    }
    
    private void RefreshScore()
    {
        priceText.text = Player.Instance.Score + " ¥";
    }
    
    private void OnStartLevel(Level _level)
    {
        Open();
        RefreshScore();
        levelName.text = GameManager.Instance.CurrentLevel.Name;
        sliders.Clear();
    }

    private void RefreshSliders(Level _level)
    {
        for (var i = 0; i < _level.SubLevels.Length; i++)
        {
            GUIItem sliderItem = sliders.GetItem(i);
            SizeSlider sizeSlider = sliderItem.GetComponent<SizeSlider>();
            int index = GameManager.Instance.GetCurrentSubLevelIndex();
            if (index > i)
            {
                sizeSlider.Value = 1;
            }
            else if (index < i)
                sizeSlider.Value = 0;
            else RefreshCurrentSlider();
        }
    }

    private void RefreshCurrentSlider()
    {
        int index = GameManager.Instance.GetCurrentSubLevelIndex();
        GUIItem sliderItem = sliders.GetItem(index);
        SizeSlider sizeSlider = sliderItem.GetComponent<SizeSlider>();
        SubLevel subLevel = GameManager.Instance.CurrentSubLevel;
        sizeSlider.Value = (float) subLevel.CurrentCommonProgress /
                           GameManager.Instance.CurrentSubLevel.MaxProgress;
    }


    private void OnEndLevel(Level _level)
    {
        Close();
    }

    private void OnTargetChanged()
    {
        RefreshTarget();
    }

    private void RefreshTarget()
    {
        Letter target = LettersDictionary.Instance[GameManager.Instance.CurrentSubLevel.Target.Index];
        targetSprite.sprite = target.Icon;
        targetText.text = target.Transcription + " " + target.Translation;
        if (BallFieldView.instance.IsInited) 
            targetSprite.color = BallFieldView.instance.BallField.GetLetterColor(target);
        letterSlider.Value = 0;

        if (GameManager.Instance.CurrentLevel.LevelType == LevelType.Exam ||
            GameManager.Instance.CurrentLevel.LevelType == LevelType.SuperExam)
        {
            targetSprite.enabled = false;
            showButton.gameObject.SetActive(true);
        }
        else
        {
            targetSprite.enabled = true;
            showButton.gameObject.SetActive(false);
        }
        
        SoundManager.Instance.PlaySound(changeTargetSount);
        targetAnimator.Play("appear");
    }
    
    #if UNITY_EDITOR

    [Button]
    [HideInEditorMode]
    public void ForceComplete()
    {
        GameManager.Instance.CurrentSubLevel.End(false);
        GameManager.Instance.CurrentLevel.Completed = true;
        GameManager.OnEndLevel.Execute(GameManager.Instance.CurrentLevel);
    }
    
    #endif
}
