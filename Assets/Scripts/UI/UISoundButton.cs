/// <summary>
/// Кнопка звука
/// </summary>
public class UISoundButton : UITickButton
{
    public override void Init()
    {
        base.Init();
        if (SoundManager.Instance) 
            RefreshView();
    }

    public override void ChangeActivation()
    {
        base.ChangeActivation();
        SoundManager.Instance.ChangeSoundActivation(!SoundManager.Instance.SoundOn);
        RefreshView();
    }

    private void RefreshView()
    {
        tickGameObject.SetActive(SoundManager.Instance.SoundOn);
        image.sprite = SoundManager.Instance.SoundOn ? activeSprite : inactiveSprite;
    }
}
