using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Отображение одного уровня
/// </summary>
public class UILevel : GUIItem
{
    [SerializeField]
    private Sprite[] levelIcons;

    [SerializeField]
    private Button button;
    
    [SerializeField]
    private Image bg;

    [SerializeField] private Image decor;

    [SerializeField]
    private Text name;

    [SerializeField] private Text price;

    public Level Level { get; private set; }

    private void Awake()
    {
        button.onClick.AddListener(TryOpenLevel);
        GameManager.OnEndLevel += OnEndLevel;
    }

    private void OnEnable()
    {
        Refresh();
    }

    private void OnEndLevel(Level _level)
    {
        if (_level == Level || _level.Number == Level.Number - 1)
        {
            TryOpenNext();
            Refresh();
        }
    }

    /// <summary>
    /// Пытается открыть следующий уровень
    /// </summary>
    private void TryOpenNext()
    {
        if (Player.Instance.UnlockedLevel == Level.Number - 1)
        {
            if (Level.Price <= Player.Instance.Score)
            {
                Level.Unlock();
            }
        }
    }

    private void OnDestroy()
    {
        button.onClick.RemoveAllListeners();
    }

    public void Init(Level _level)
    {
        Level = _level;
        name.text = _level.Name;
        price.text = _level.Price + " ¥";
        name.color = _level.LevelType == LevelType.Training || _level.SubLevels.All(x => !x.UseColor) ? Color.black : Color.white;
        price.color = _level.LevelType == LevelType.Training || _level.SubLevels.All(x => !x.UseColor) ? Color.black : Color.white;
        decor.sprite = _level.Image;
        Refresh();
    }

    private void Refresh()
    {
        if (Level == null)
            return;
        
        price.gameObject.SetActive(!Level.Unlocked);
        bg.sprite = Level.Completed ? levelIcons[0] :
            Level.Number == Player.Instance.UnlockedLevel ? levelIcons[1] : levelIcons[2];
    }

    private void TryOpenLevel()
    {
        if (Player.Instance.CompletedLevel < Level.Number - 1)
        {
            //предыдущий уровень не завершен
            return;
        }

        if (!Level.Unlocked)
        {
            if (Level.Price > Player.Instance.Score)
            {
                string message = TextManager.Instance.GetDisabledLevelText(Level.Number);
                if (!string.IsNullOrEmpty(message))
                    (GUIManager.Instance.GetWindow<TextHint>() as TextHint).ShowImmideatly(message);
                return;
            }
            Level.Unlock();
        }
        
        if (Level.SubLevels.Length == 0)
        {
            (GUIManager.Instance.GetWindow<TextHint>() as TextHint).ShowImmideatly(
                "У тебя хорошо получается! Скоро тебя ждут новые приключения! После битвы Золотому дракону нужно набраться сил. И ты отдохни. Но береги сундук знаний и периодически заглядывай в него. Это позволит тебе вместе с Золотым драконом полностью освободить мир от тьмы.");
            return;
        }

        GameManager.Instance.LoadLevel(Level);
    }
}
