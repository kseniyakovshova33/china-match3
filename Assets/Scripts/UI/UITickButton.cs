using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Кнопка с галочкой
/// </summary>
public class UITickButton : MonoBehaviour
{
    [SerializeField]
    protected Image image;
    
    [SerializeField]
    protected Sprite activeSprite, inactiveSprite;

    [SerializeField]
    protected GameObject tickGameObject;

    [SerializeField]
    private Button button;
    

    public virtual void Init()
    {
        button.onClick.AddListener(ChangeActivation);
    }

    public virtual void DeInit()
    {
        button.onClick.RemoveAllListeners();
    }

    public virtual void ChangeActivation()
    {
        
    }
}
