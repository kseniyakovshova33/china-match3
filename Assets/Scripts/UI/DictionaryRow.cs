using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Строка словаря
/// </summary>
public class DictionaryRow : GUIItem
{
    [SerializeField]
    private Image letterImage;

    [SerializeField]
    private Text letterDescription;

    [SerializeField]
    private Button playSoundButton;

    private Letter letter;

    public void Init(Letter _letter)
    {
        base.Init();
        letterImage.sprite = _letter.Icon;
        letterDescription.text = _letter.Transcription + " " + _letter.Translation;
        playSoundButton.onClick.AddListener(PlaySound);
        letter = _letter;
    }

    private void PlaySound()
    {
        SoundManager.Instance.PlaySound(letter.Pronunciation);
    }

    public override void Deinit()
    {
        base.Deinit();
        playSoundButton.onClick.RemoveAllListeners();
    }
}
