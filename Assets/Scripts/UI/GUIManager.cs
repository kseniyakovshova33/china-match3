using UnityEngine;

public class GUIManager : MonoBehaviour
{
    private WindowBase[] windows;

    public static GUIManager Instance;
    
    private void Awake()
    {
        Instance = this;
        windows = GetComponentsInChildren<WindowBase>(true);
        foreach (WindowBase window in windows)
        {
            if (!window.gameObject.activeSelf)
            {
                window.gameObject.SetActive(true);
                window.gameObject.SetActive(false);
            }
        }
    }

    public WindowBase GetWindow<T>()
    {
        foreach (WindowBase window in windows)
        {
            if (window is T)
                return window;
        }

        return null;
    }
    
    public WindowBase GetWindow(string _typeName)
    {
        foreach (WindowBase window in windows)
        {
            if (window.GetType().ToString() == _typeName)
                return window;
        }

        return null;
    }

    public void CloseAll()
    {
        foreach (WindowBase window in windows)
        {
            if (window.gameObject.activeSelf)
                window.Close();
        }
    }
}
