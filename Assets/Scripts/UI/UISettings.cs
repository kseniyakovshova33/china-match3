using UnityEngine;

/// <summary>
/// Окно настроек
/// </summary>
public class UISettings : WindowBase
{
    [SerializeField]
    private UITickButton[] tickButtons;

    private void OnEnable()
    {
        foreach (UITickButton tickButton in tickButtons)
        {
            tickButton.Init();
        }
    }

    private void OnDisable()
    {
        foreach (UITickButton tickButton in tickButtons)
        {
            tickButton.DeInit();
        }
    }
}
