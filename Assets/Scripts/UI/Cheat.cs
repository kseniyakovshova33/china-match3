using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Подсказка матча
/// </summary>
public class Cheat : MonoBehaviour
{
    [SerializeField]
    private GameObject handPrefab;

    /// <summary>
    /// Уровни, на которых разрешена подсказка
    /// </summary>
    [SerializeField]
    private int[] levelsAllowedCheat;
    
    private GameObject hand;

    private Coroutine matchCoroutine;

    private void Awake()
    {
        GameManager.OnStartLevel += OnStartLevel;
        GameManager.OnEndLevel += OnEndLevel;
    }

    private void OnMatch(List<Ball> _balls)
    {
        if (hand != null)
            DestroyImmediate(hand);
        if (matchCoroutine != null)
            StopCoroutine(matchCoroutine);
        matchCoroutine = StartCoroutine(MatchCoroutine());
    }

    private void OnStartLevel(Level _level)
    {
        if (levelsAllowedCheat.Contains(_level.Number))
        {
            BallField.OnMatch += OnMatch;
            matchCoroutine = StartCoroutine(MatchCoroutine());
        }
    }
    
    private void OnEndLevel(Level _level)
    {
        BallField.OnMatch -= OnMatch;
        if (matchCoroutine != null)
            StopCoroutine(matchCoroutine);
    }

    private IEnumerator MatchCoroutine()
    {
        yield return new WaitForSeconds(10);
            
            hand = Instantiate(handPrefab, BallFieldView.instance.BallsPool);
            List<Ball> result;
            BallFieldView.instance.BallField.FindPotentialMatch(out result);
            
            Ball targetBall = result.First();
            Ball endBall = null;

            Ball closest = null;
            float minDistance = float.MaxValue;
            foreach (Ball ball in result)
            {
                if (ball != targetBall)
                {
                    float distance = Vector2.Distance(targetBall.PositionIndex, ball.PositionIndex);
                    if (distance < minDistance)
                    {
                        minDistance = distance;
                        closest = ball;
                    }
                }
            }

            var groups = result.GroupBy(x => x.PositionIndex.x);

            if (groups.Count() <= 2)
            {
                endBall = BallFieldView.instance.BallField.GetNeighbour(closest, closest.PositionIndex.y > targetBall.PositionIndex.y ? Direction.Up : Direction.Down);
            }
            else endBall = BallFieldView.instance.BallField.GetNeighbour(closest, closest.PositionIndex.x > targetBall.PositionIndex.x ? Direction.Left : Direction.Right);

            Vector2 start = BallFieldView.instance.GetBallViewPosition(targetBall.PositionIndex) + new Vector2(BallFieldView.instance.CellSize.x/2, -BallFieldView.instance.CellSize.y);
            Vector2 end = BallFieldView.instance.GetBallViewPosition(endBall.PositionIndex) + new Vector2(BallFieldView.instance.CellSize.x/2, -BallFieldView.instance.CellSize.y);
            hand.GetComponent<TweenPosition>().@from = start;
            hand.GetComponent<TweenPosition>().to = end;
            while (gameObject.activeSelf)
            {
                yield return new WaitForSeconds(1);
                hand.GetComponent<TweenColor>().enabled = true;
                hand.GetComponent<TweenColor>().PlayForward();
                yield return new WaitForSeconds(1);
                hand.GetComponent<TweenPosition>().ResetToBeginning();
                hand.GetComponent<TweenPosition>().PlayForward();
                hand.GetComponent<TweenColor>().ResetToBeginning();
            }
            DestroyImmediate(hand);
    }

    private void OnDestroy()
    {
        GameManager.OnStartLevel -= OnStartLevel;
        GameManager.OnEndLevel -= OnEndLevel;
    }
}
