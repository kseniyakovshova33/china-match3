/// <summary>
/// Кнопка музыки
/// </summary>
public class UIMusicButton : UITickButton
{
    public override void Init()
    {
        base.Init();
        if (SoundManager.Instance)
            RefreshView();
    }

    public override void ChangeActivation()
    {
        base.ChangeActivation();
        SoundManager.Instance.ChangeMusicActivation(!SoundManager.Instance.MusicOn);
        RefreshView();
    }

    private void RefreshView()
    {
        tickGameObject.SetActive(SoundManager.Instance.MusicOn);
        image.sprite = SoundManager.Instance.MusicOn ? activeSprite : inactiveSprite;
    }
}