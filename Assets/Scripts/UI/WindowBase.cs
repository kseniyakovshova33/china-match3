using System;
using System.Collections;
using UnityEngine;

public class WindowBase : MonoBehaviour
{
    [SerializeField]
    private Animator animator;

    public static Action<WindowBase> OnOpen;
    public static Action<WindowBase> OnClose;
    
    private WindowBase prevWindow;
    
    public bool Opened { get; private set; }

    public void Open(bool _closeAll = true, WindowBase _prevWindow = null)
    {
        if (_prevWindow != null)
            prevWindow = _prevWindow;
        
        if (_closeAll)
        {
            GUIManager.Instance.CloseAll();
        }

        StopCoroutine(CloseDelayed());
        gameObject.SetActive(true);
        if (animator != null)
            animator.SetBool("show", true);

        Opened = true;
        OnOpen.Execute(this);
    }

    private IEnumerator CloseDelayed()
    {
        yield return new WaitForSeconds(0.1f); //TODO вместо константы длительность анимации
        gameObject.SetActive(false);
    }

    public void Close()
    {
        if (animator != null)
        {
            animator.SetBool("show", false);
            if (gameObject.activeSelf)
                StartCoroutine(CloseDelayed());
        }
        else gameObject.SetActive(false);

        if (prevWindow != null)
        {
            prevWindow.Open();
            prevWindow = null;
        }
        Opened = false;
        OnClose.Execute(this);
    }
}
