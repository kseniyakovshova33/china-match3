using Sirenix.OdinInspector;
using UnityEngine;

/// <summary>
/// Базовый класс данных игрока
/// </summary>
public class Player : MonoBehaviour
{
    [SerializeField]
    private LevelManager levelManager;

    [SerializeField]
    private LettersDictionary lettersDictionary;

    [SerializeField]
    private TextManager textManager;
    
    [SerializeField]
    private SoundManager soundManager;

    [SerializeField]
    private Tutorial tutorial;

    /// <summary>
    /// Индекс максимального разблокированного уровня
    /// </summary>
    public int UnlockedLevel
    {
        get
        {
            int maxLevel = 0;
            foreach (Level level in LevelManager.Instance)
            {
                if (level.Unlocked)
                    maxLevel = level.Number;
            }

            return maxLevel;
        }
    }

    /// <summary>
    /// Индекс максимального завершенного уровня
    /// </summary>
    public int CompletedLevel
    {
        get
        {
            int maxLevel = 0;
            foreach (Level level in LevelManager.Instance)
            {
                if (level.Completed)
                    maxLevel = level.Number;
            }

            return maxLevel;
        }
    }
    private int defaultScore = 0;

    public int Score { get; set; }

    public static Player Instance;

    private void Awake()
    {
        Instance = this;
        levelManager.Init();
        lettersDictionary.Init();
        textManager.Init();
        Load();
        tutorial.Init();
    }

    private void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
            Save();
    }

    private void OnApplicationQuit()
    {
        Save();
    }

    private void Save()
    {
        soundManager.Save();
        textManager.Save();
        foreach (Level level in levelManager)
        {
            level.Save();
        }
        PlayerPrefs.SetInt("score", Score);
        PlayerPrefs.Save();
    }

    private void Load()
    {
        soundManager.Load();
        textManager.Load();
        foreach (Level level in levelManager)
        {
            level.Load();
        }

        Score = PlayerPrefs.GetInt("score", defaultScore);
    }

    [Button]
    private void ResetProfile()
    {
        PlayerPrefs.DeleteAll();
    }
    
    [Button]
    private void Add100()
    {
        Score += 100;
    }
}
