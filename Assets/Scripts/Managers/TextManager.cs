using UnityEngine;

/// <summary>
/// Менеджер текстового сопровождения игры
/// </summary>
[CreateAssetMenu(fileName = "TextManager", menuName = "ScriptableObjects/TextManager")]
public class TextManager : ScriptableObject
{
    [SerializeField]
    private TextData[] textDatas;

    public static TextManager Instance;

    public void Init()
    {
        Instance = this;
        foreach (TextData textData in textDatas)
        {
            textData.Init();
        }
    }

    public string GetDisabledLevelText(int _levelNumber)
    {
        foreach (TextData textData in textDatas)
        {
            if (textData.ShowCondition is LevelEndedCondition levelEndedCondition)
            {
                if (_levelNumber - 1 == levelEndedCondition.Level && levelEndedCondition.IfNextLevelDisabled)
                    return textData.Text;
            }
        }

        return string.Empty;
    }

    public void Save()
    {
        for (int i = 0; i < textDatas.Length; i++)
        {
            PlayerPrefs.SetInt("text"+i, textDatas[i].Shown ? 1 : 0);
        }
        PlayerPrefs.Save();
    }

    public void Load()
    {
        for (int i = 0; i < textDatas.Length; i++)
        {
            textDatas[i].Shown = PlayerPrefs.GetInt("text"+i, 0) == 1;
        }
        PlayerPrefs.Save();
    }
}


