using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Менеджер матч3
/// </summary>
public class GameManager : MonoBehaviour
{
    public Level CurrentLevel { get; private set; }

    public SubLevel CurrentSubLevel { get; private set; }

    public static Action<Level> OnStartLevel;
    public static Action<Level> OnEndLevel;

    public static GameManager Instance;
    
    private List<LetterData> usedLetters = new List<LetterData>();

    private void Awake()
    {
        Instance = this;
        SubLevel.OnEndSubLevel += OnEndSubLevel;
    }

    private void OnEndSubLevel(SubLevel _subLevel, bool _success)
    {
        if (_success)
        {
            int index = Array.IndexOf(CurrentLevel.SubLevels, CurrentSubLevel);
            if (index < CurrentLevel.SubLevels.Length - 1)
            {
                StartCoroutine(NextSubLevel(index));

            }
            else
            {
                CurrentLevel.Completed = true;
                OnEndLevel?.Invoke(CurrentLevel);
            }
        }
    }

    private IEnumerator NextSubLevel(int _index)
    {
        yield return new WaitForSeconds(1f);
        CurrentSubLevel = CurrentLevel.SubLevels[_index + 1];
        CurrentSubLevel.Start(GetUnusedLetters());
    }

    private LetterData[] GetUnusedLetters()
    {
        LetterData[] result = new LetterData[CurrentSubLevel.LetterCount];
        for (int i = 0; i < result.Length; i++)
        {
            LetterData randomLetter = CurrentLevel.Letters.Except(usedLetters).ToList().GetRandom();
            result[i] = randomLetter ?? CurrentLevel.Letters.GetRandom();
            usedLetters.Add(randomLetter);
        }

        return result;
    }

    private void OnDestroy()
    {
        SubLevel.OnEndSubLevel -= OnEndSubLevel;
    }

    public void LoadLevel(Level _level)
    {
        usedLetters.Clear();
        
        CurrentLevel = _level;
        CurrentSubLevel = CurrentLevel.SubLevels[0];
        OnStartLevel?.Invoke(CurrentLevel);
        CurrentSubLevel.Start(GetUnusedLetters());
    }

    public int GetCurrentSubLevelIndex()
    {
        return Array.IndexOf(CurrentLevel.SubLevels, CurrentSubLevel);
    }
}
