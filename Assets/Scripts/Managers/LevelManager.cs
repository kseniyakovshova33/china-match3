using System.Collections;
using System.Linq;
using UnityEngine;

/// <summary>
/// Менеджер уровней
/// </summary>
[CreateAssetMenu(fileName = "LevelManager", menuName = "ScriptableObjects/LevelManager")]
public class LevelManager : ScriptableObject
{
    [SerializeField]
    private Level[] levels;
    
    public Level this[int _index] => levels[_index];
    
    public static LevelManager Instance;
    
    public IEnumerator GetEnumerator()
    {
        return levels.GetEnumerator();
    }

    public Level GetLevelByNumber(int _levelNumber)
    {
        return levels.FirstOrDefault(x => x.Number == _levelNumber);
    }

    public void Init()
    {
        Instance = this;
    }
}
