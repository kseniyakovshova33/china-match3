using System;
using System.Collections;
using System.Linq;
using UnityEngine;

/// <summary>
/// Словарь иероглифов
/// </summary>
[CreateAssetMenu(fileName = "LettersDictionary", menuName = "ScriptableObjects/LettersDictionary")]
public class LettersDictionary : ScriptableObject, IEnumerable
{
    [SerializeField]
    private Letter[] letters;

    public Letter this[int _index] => letters[_index];
    
    public Letter this[string _translation] => letters.FirstOrDefault(x => x.Translation == _translation);
    
    public IEnumerator GetEnumerator()
    {
        return letters.GetEnumerator();
    }

    public static LettersDictionary Instance;

    public void Init()
    {
        Instance = this;
    }


#if UNITY_EDITOR

    private void OnValidate()
    {
        foreach (Letter letter in letters)
        {
            if (letter.Id == -1)
                letter.Id = Array.IndexOf(letters, letter);
        }
    }

#endif
}
