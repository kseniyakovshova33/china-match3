using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// Менеджер звуков
/// </summary>
public class SoundManager : MonoBehaviour
{
    [SerializeField] private AudioSource audioSourcePrefab;
    
    [SerializeField] private AudioSource mainMelodySource = null;
    
    [SerializeField] private AudioClip startSound, endLevelSound;

    [SerializeField]
    private AudioClip[] mainMelodies;

    public bool SoundOn { get; set; } = true;

    public bool MusicOn { get; set; }

    public static SoundManager Instance;

    private void Awake()
    {
        Instance = this;
        PlayMainMusic();
        GameManager.OnEndLevel += OnEndLevel;
    }

    private void OnDestroy()
    {
        GameManager.OnEndLevel -= OnEndLevel;
    }
    
    private void OnEndLevel(Level _level)
    {
        PlaySound(endLevelSound);
    }

    public void PlaySound(AudioClip _audioClip)
    {
        if (SoundOn)
        {
            AudioSource audioSource = Instantiate(audioSourcePrefab);
            audioSource.clip = _audioClip;
            audioSource.Play();
            StartCoroutine(DestroyAudioSource(audioSource, audioSource.clip.length));
        }
    }

    private IEnumerator DestroyAudioSource(AudioSource _audioSource, float _delay)
    {
        yield return new WaitForSeconds(_delay);
        Destroy(_audioSource.gameObject);
    }

    public void PlayMainMusic()
    {
        if (MusicOn)
            StartCoroutine(PlayMusic());
    }

    private IEnumerator PlayMusic()
    {
        while (MusicOn)
        {
            int melodyIndex = Array.IndexOf(mainMelodies, mainMelodySource.clip);
            AudioClip currentClip = mainMelodies[melodyIndex == -1 || melodyIndex == mainMelodies.Length - 1 ? 0 : melodyIndex + 1];
            mainMelodySource.clip = currentClip;
            mainMelodySource.Play();
            yield return new WaitForSeconds(currentClip.length);
        }
    }

    public void ChangeSoundActivation(bool _active)
    {
        SoundOn = _active;
    }
    
    public void ChangeMusicActivation(bool _active)
    {
        MusicOn = _active;
        if (!_active)
        {
            mainMelodySource.Stop();
            StopCoroutine(PlayMusic());
        }
        else PlayMainMusic();
    }

    public void Save()
    {
        PlayerPrefs.SetInt("sound", SoundOn ? 1 : 0);
        PlayerPrefs.SetInt("music", MusicOn ? 1 : 0);
        PlayerPrefs.Save();
    }

    public void Load()
    {
        if (!PlayerPrefs.HasKey("sound"))
            PlaySound(startSound);
        SoundOn = PlayerPrefs.GetInt("sound", 1) == 1;
        MusicOn = PlayerPrefs.GetInt("music", 0) == 1;
    }
}
