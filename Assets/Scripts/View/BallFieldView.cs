using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

/// <summary>
/// View игрового поля
/// </summary>
public class BallFieldView : MonoBehaviour
{
    [SerializeField]
    private Color[] availiableColors;

    [SerializeField]
    private RectTransform ballsPool;

    [SerializeField]
    private BallView ballPrefab;

    [SerializeField]
    private Vector2 cellSize;

    public Vector2 CellSize => cellSize;

    public RectTransform BallsPool => ballsPool;

    public BallField BallField { get; private set; }

    private List<BallView> pool = new List<BallView>();

    public static BallFieldView instance;

    public static Action OnInit;
    public static Action OnWrongMove;

    public bool IsInited => instance != null && instance.BallField != null;
    
    private void Awake()
    {
        SubLevel.OnStartSubLevel += OnStartSubLevel;
        SubLevel.OnEndSubLevel += OnEndSubLevel;
        GameManager.OnEndLevel += OnEndLevel;
        BallView.OnTryMove += OnTryMove;
        Ball.OnCreate += OnCreate;
        instance = this;
    }

    private void OnEndLevel(Level _level)
    {
        foreach (BallView ballView in pool)
        {
            Destroy(ballView.gameObject);
        }
        pool.Clear();
    }

    private void OnDestroy()
    {
        SubLevel.OnStartSubLevel -= OnStartSubLevel;
        SubLevel.OnEndSubLevel -= OnEndSubLevel;
        GameManager.OnEndLevel -= OnEndLevel;
        Ball.OnCreate -= OnCreate;
        BallView.OnTryMove -= OnTryMove;
    }

    public void OnStartSubLevel(SubLevel _subLevel)
    {
        BallField = new BallField();
        BallField.OnGenerated += OnGenerated;
        BallField.OnRemoved += OnRemoved;
        BallField.Generate(_subLevel, _subLevel.UseColor ? availiableColors : new []{Color.grey});
        OnInit.Execute();
    }

    public void OnEndSubLevel(SubLevel _subLevel, bool _success)
    {
        BallField.OnGenerated -= OnGenerated;
        BallField.OnRemoved -= OnRemoved;
        BallField = null;

        if (!_success)
        {
            OnEndLevel(GameManager.Instance.CurrentLevel);
        }
    }

    private void OnRemoved(Ball _ball)
    {
        
    }
    
    private void OnCreate(Ball _ball)
    {
        BallView ballView = pool.Find(x => x.Ball == _ball);
        if (ballView != null && BallField != null)
        {
            ballView.transform.localPosition = GetBallViewPosition(new Vector2Int(_ball.PositionIndex.x,
                _ball.PositionIndex.y - BallField.FieldSize.y));
            gameObject.SetActive(true);
            ballView.PlayMoving(GetBallViewPosition(_ball.PositionIndex), BallField.RemoveAllMatches);
        }
    }
    
    private void OnTryMove(BallView _ballView, Direction _direction)
    {
        Ball neighbour = BallField.GetNeighbour(_ballView.Ball, _direction);
        if (neighbour == null)
            return;
        
        BallView neighbourView = pool.Find(x => x.Ball == neighbour);
        BallField.Change(_ballView.Ball, neighbour, () => OnMoveFinished(_ballView, neighbourView));
    }

    private void OnMoveFinished(BallView _ball1, BallView _ball2)
    {
        List<Ball> match;
        BallField.FindMatch(out match);
        if (match.Count == 0) 
        {
            //возвращаем шарики обратно
            BallField.Change(_ball1.Ball, _ball2.Ball);
            OnWrongMove.Execute();
        }
        else BallField.RemoveAllMatches();
    }


    private void OnGenerated(BallField _ballField)
    {
        if (_ballField == BallField)
            RefreshField(_ballField.Balls);
    }

    private void RefreshField(Ball[] _balls)
    {
        foreach (BallView ballView in pool)
        {
            ballView.gameObject.SetActive(false);
            ballView.DeInit();
        }

        for (var i = 0; i < _balls.Length; i++)
        {
            Ball ball = _balls[i];
            if (ball.Removed)
                continue;
            
            BallView ballView = null;
            if (i >= pool.Count)
                ballView = Instantiate(ballPrefab, ballsPool);
            else
            {
                ballView = pool[i];
            }

            ballView.transform.localPosition = GetBallViewPosition(ball.PositionIndex);
            ballView.Init(ball, this);
            pool.Add(ballView);
        }
    }

    public Vector2 GetBallViewPosition(Vector2Int _positionIndex)
    {
        return new Vector3(_positionIndex.x * cellSize.x + ballsPool.rect.x,
            _positionIndex.y * (-cellSize.y) - ballsPool.rect.y);
    }

#if UNITY_EDITOR
    
    [Button]
    [HideInEditorMode]
    public void FindPotentialMatch()
    {
        List<Ball> result;
        if (BallField.FindPotentialMatch(out result))
        {
            foreach (Ball ball in result)
            {
                Debug.Log(ball.PositionIndex);
            }
        }
    }

    [Button]
    [HideInEditorMode]
    public void Mix()
    {
        BallField.Mix();
    }

#endif
}
