using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// View шарика на поле
/// </summary>
public class BallView : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    [SerializeField]
    private Image letterImage;

    [SerializeField]
    private TweenPosition tweenPosition;

    [SerializeField]
    private Animator animator;
    
    public Ball Ball { get; set; }

    private BallFieldView owner;

    private bool isDragging;

    private bool isMoving;

    public static Action<BallView, Direction> OnTryMove;

    private Action onMoveFinished;

    private void Awake()
    {
        Ball.OnRemoved += OnRemoved;
        SubLevel.OnEndSubLevel += OnEndSubLevel;
    }

    private void OnDestroy()
    {
        Ball.OnRemoved -= OnRemoved;
        SubLevel.OnEndSubLevel -= OnEndSubLevel;
    }

    public void Init(Ball _ball, BallFieldView _ballFieldView)
    {
        Ball = _ball;
        owner = _ballFieldView;
        letterImage.sprite = _ball.Letter.Icon;
        letterImage.color = _ball.Color;
        Ball.OnPositionChanged += OnPositionChanged;
        animator.ResetTrigger("destroy");
        gameObject.SetActive(true);
    }

    private void OnEndSubLevel(SubLevel _subLevel, bool _success)
    {
        onMoveFinished = null;
    }

    public void DeInit()
    {
        if (Ball != null)
            Ball.OnPositionChanged -= OnPositionChanged;
        Ball = null;
        owner = null;
        isMoving = false;
    }

    public void PlayMoving(Vector2 _endPosition, Action _onFinished = null)
    {
        tweenPosition.@from = transform.localPosition;
        tweenPosition.to = _endPosition;
        tweenPosition.duration = Vector2.Distance(_endPosition, transform.localPosition)/ owner.CellSize.y * 0.2f;
        tweenPosition.ResetToBeginning();
        tweenPosition.PlayForward();
        isMoving = true;
        onMoveFinished = () =>
        {
            _onFinished.Execute();
            isMoving = false;
        };
        tweenPosition.SetOnFinished(() => { onMoveFinished?.Invoke(); });
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (!isDragging || isMoving)
            return;

        Vector2 changing = (eventData.position - eventData.pressPosition);
        bool isCanceled = eventData == null || changing.magnitude <= 0;
        if (isCanceled)
            return;

        OnTryMove?.Invoke(this, GetDirection(changing));
        isDragging = false;
    }

    private Direction GetDirection(Vector2 _changing)
    {
        if (Math.Abs(_changing.x) >= Math.Abs(_changing.y))
            return _changing.x > 0 ? Direction.Right : Direction.Left;
        return _changing.y > 0 ? Direction.Up : Direction.Down;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        isDragging = true;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        isDragging = false;
    }
    
    private void OnRemoved(Ball _ball)
    {
        if (Ball == _ball)
        {
            gameObject.SetActive(false);
            //animator.SetTrigger("destroy");
        }
        //TODOPlayEffect
    }

    private void OnPositionChanged(Vector2Int _newPosition, Action _onFinished)
    {
        PlayMoving(owner.GetBallViewPosition(_newPosition), _onFinished);
    }
}
