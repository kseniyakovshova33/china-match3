using System;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

/// <summary>
/// Иероглиф
/// </summary>
[Serializable]
public class Letter
{
    [ShowInInspector] 
    [ReadOnly]
    public int Id { get; set; } = -1;
    
    [SerializeField]
    private Sprite icon;

    public Sprite Icon => icon;

    [SerializeField]
    private string translation;

    public string Translation => translation;

    [SerializeField]
    private string transcription;

    public string Transcription => transcription;

    [SerializeField]
    private AudioClip pronunciation;

    public AudioClip Pronunciation => pronunciation;

    public bool IsUnlocked()
    {
        for (int i = 0; i < Player.Instance.CompletedLevel; i++)
        {
            foreach (LetterData letterData in LevelManager.Instance[i].Letters)
            {
                if (LettersDictionary.Instance[letterData.Index] == this)
                    return true;
            }
        }

        return false;
    }
}
