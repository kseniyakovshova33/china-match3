using System;
using UnityEngine;

/// <summary>
/// Шар - класс модели
/// </summary>
public class Ball
{
    public Letter Letter{ get; private set; }
    
    public Color Color { get; private set; }
    
    public Vector2Int PositionIndex { get; private set; }
    
    public bool Removed { get; set; }

    public static Action<Ball> OnRemoved;
    
    public static Action<Ball> OnCreate;
    
    public Action<Vector2Int, Action> OnPositionChanged;

    public void Init(Letter _letter, Color _color, Vector2Int _positionIndex, bool _playAppearence = false)
    {
        Letter = _letter;
        Color = _color;
        PositionIndex = _positionIndex;
        Removed = false;
        if (_playAppearence)
            OnCreate?.Invoke(this);
    }

    public void Remove()
    {
        Removed = true;
        OnRemoved?.Invoke(this);
    }

    public void SetPosition(Vector2Int _position, Action _onAfterMoving = null)
    {
        PositionIndex = _position;
        OnPositionChanged?.Invoke(PositionIndex, _onAfterMoving);
    }
}
