using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Подуровень
/// </summary>
[Serializable]
public class SubLevel
{ 
    public LetterData Target { get; private set; }

    /// <summary>
    /// Сколько очков дают за один матч иероглифов
    /// </summary>
    [SerializeField]
    private int matchPrice = 1;

    public int MatchPrice => matchPrice;
    
    /// <summary>
    /// Сколько очков дают за один матч ЦЕЛЕВЫХ иероглифов
    /// </summary>
    [SerializeField]
    private int targetMatchPrice;

    public int TargetMatchPrice => targetMatchPrice;

    /// <summary>
    /// Использовать ли цвета
    /// </summary>
    [SerializeField]
    private bool useColor = true;

    public bool UseColor => useColor;

    /// <summary>
    /// Количество используемых букв
    /// </summary>
    private int letterCount = 5;

    public int LetterCount => letterCount;

    /// <summary>
    /// Сколько нужно матчей каждой буквы
    /// </summary>
    [SerializeField]
    private int needMatch = 3;
    
    public int NeedMatch => needMatch;

    /// <summary>
    /// Прогресс по текущей букве
    /// </summary>
    public int CurrentLetterProgress { get; private set; }

    /// <summary>
    /// Общий прогресс
    /// </summary>
    public int CurrentCommonProgress => completedTargets.Count*NeedMatch + CurrentLetterProgress;

    private LetterData[] usingLetters;

    public Letter[] GetUsingLetters() => usingLetters.Select(x => LettersDictionary.Instance[x.Index]).ToArray();
    
    /// <summary>
    /// Максимально возможный прогресс
    /// </summary>
    public int MaxProgress => usingLetters.Length * NeedMatch;

    private List<LetterData> completedTargets = new List<LetterData>();

    public static Action OnTargetChanged;
    public static Action<SubLevel> OnStartSubLevel;
    public static Action<SubLevel, bool> OnEndSubLevel;

    public void Start(LetterData[] _usingLetters)
    {
        usingLetters = _usingLetters;
        completedTargets.Clear();
        Target = null;
        NextTarget();
        OnStartSubLevel?.Invoke(this);
        BallField.OnMatch += OnMatch;
    }

    private void OnMatch(List<Ball> _match)
    {
        if (_match.Find(x => x.Letter == LettersDictionary.Instance[Target.Index]) != null)
        {
            CurrentLetterProgress++;
            GameManager.Instance.StartCoroutine(CheckComplete());
        }
    }

    private IEnumerator CheckComplete()
    {
        yield return new WaitForSeconds(1f);
        if (CurrentLetterProgress >= NeedMatch)
        {
            NextTarget();
            if (Target != null)
                OnTargetChanged?.Invoke();
            else End();
        }
    }

    public void NextTarget()
    {
        if (Target != null)
            completedTargets.Add(Target);
        if (completedTargets.Count == usingLetters.Length)
            Target = null;
        else Target = usingLetters.Except(completedTargets).ToList().GetRandom();
        CurrentLetterProgress = 0;
    }

    public void End(bool _success = true)
    {
        completedTargets.Clear();
        BallField.OnMatch -= OnMatch;
        OnEndSubLevel?.Invoke(this, _success);
        Target = null;
    }
}

[Serializable]
public class LetterData
{
    [LetterEntry]
    public int Index;
}

