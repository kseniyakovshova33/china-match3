using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = System.Random;

/// <summary>
/// Модель игрового поля
/// </summary>
public class BallField
{
    /// <summary>
    /// Все доступные шары
    /// </summary>
    private Ball[] balls;

    public Ball[] Balls => balls;

    /// <summary>
    /// Размер игрового поля
    /// </summary>
    private Vector2Int fieldSize = new Vector2Int(6,6);

    public Vector2Int FieldSize => fieldSize;

    public static Action<BallField> OnGenerated;

    public static Action<List<Ball>> OnMatch;
    
    public static Action OnNoMoves;
    
    public event Action<Ball> OnRemoved;

    private Letter[] usingLetters;
    
    private Dictionary<Letter, Color> letterToColor = new Dictionary<Letter, Color>();

    private Coroutine delayCollapse;

    public void Generate(SubLevel _subLevel, Color[] _availiableColors)
    {
        balls = new Ball[fieldSize.x * fieldSize.y];
        usingLetters = _subLevel.GetUsingLetters();
        letterToColor.Clear();
        
        int index = 0;
        foreach (Letter letter in usingLetters)
        {
            letterToColor.Add(letter, index < _availiableColors.Length ? _availiableColors[index] : _availiableColors.Last());
            index++;
        }
        
        for (int i = 0; i < balls.Length; i++)
        {
            Letter letter = usingLetters.GetRandom();
            balls[i] = new Ball();
            balls[i].Init(letter, letterToColor[letter], new Vector2Int(i%fieldSize.x, (int)(i/fieldSize.x)) );
        }

        ResetMatches();
        
        OnGenerated?.Invoke(this);
    }

    public Color GetLetterColor(Letter _letter)
    {
        return letterToColor[_letter];
    }

    /// <summary>
    /// Удаляет все доступные матчи
    /// </summary>
    public void RemoveAllMatches()
    {
        List<Ball> match = new List<Ball>();
        do
        {
            FindMatch(out match);
            if (match != null)
            {
                for (int i = 0; i < match.Count; i++)
                {
                    Vector2Int pos = match[i].PositionIndex;
                    int index = GetBallIndexByPosition(pos);
                    if (index >= 0)
                    {
                        balls[index].Remove();
                    }
                }

                OnMatch?.Invoke(match);
            }
        } while (match.Count > 0);


        Collapse();

        foreach (Ball ball in balls)
        {
            if (ball.Removed)
            {
                ReplaceBall(ball);
            }
        }

        List<Ball> potentialMatch;
        if (!FindPotentialMatch(out potentialMatch))
        {
            OnNoMoves.Execute();
            Mix(); 
        }

        OnGenerated.Execute(this);
    }

    public void Mix()
    {
        Random random = new Random();
        int n = balls.Length;
        while (n > 1) 
        {
            int k = random.Next(n--);
            Vector2Int temp = balls[n].PositionIndex;
            balls[n].SetPosition(balls[k].PositionIndex);
            if (n == 1)
                balls[k].SetPosition(temp, RemoveAllMatches);
            else balls[k].SetPosition(temp);
        }
    }

    /// <summary>
    /// Создает новый шарик вместо удаленного
    /// </summary>
    /// <param name="_ball"></param>
    private void ReplaceBall(Ball _ball)
    {
        Letter letter = usingLetters.GetRandom();
        _ball.Init(letter, letterToColor[letter],
            new Vector2Int(_ball.PositionIndex.x,
                balls.Count(x =>
                    x.PositionIndex.x == _ball.PositionIndex.x &&
                    x.Removed) - 1), true);
    }

    /// <summary>
    /// Сдвинуть шарики на освободившиеся места
    /// </summary>
    private void Collapse()
    {
        foreach (Ball ball in balls)
        {
            if (ball.Removed)
                continue;

            int removedCount = balls.Count(x =>
                x.Removed && x.PositionIndex.x == ball.PositionIndex.x && x.PositionIndex.y > ball.PositionIndex.y);
            ball.SetPosition(new Vector2Int(ball.PositionIndex.x, ball.PositionIndex.y + removedCount));
        }
    }

    /// <summary>
    /// Сбросить все существующие матчи
    /// </summary>
    public void ResetMatches()
    {
        List<Ball> match;
        while (FindMatch(out match))
        {
            for (int i = 1; i < match.Count; i+=2)
            {
                List<Letter> copy = usingLetters.ToList();
                copy.Remove(match[i].Letter);
                Letter letter = copy.GetRandom();
                match[i].Init(letter, letterToColor[letter], match[i].PositionIndex);
            }
        }
    }

    /// <summary>
    /// Получить индекс шарика в массиве по его расположению
    /// </summary>
    /// <param name="_positionIndex"></param>
    /// <returns></returns>
    public int GetBallIndexByPosition(Vector2Int _positionIndex)
    {
        return Array.FindIndex(balls, x => x.PositionIndex == _positionIndex);
    }

    /// <summary>
    /// Получает соседа по направлению
    /// </summary>
    /// <param name="_ball"></param>
    /// <param name="_direction"></param>
    /// <returns></returns>
    public Ball GetNeighbour(Ball _ball, Direction _direction)
    {
        int index = -1;
        switch (_direction)
        {
            case Direction.Left:
            {
                index = GetBallIndexByPosition(new Vector2Int(_ball.PositionIndex.x - 1, _ball.PositionIndex.y));
                break;
            }

            case Direction.Right:
            {
                index = GetBallIndexByPosition(new Vector2Int(_ball.PositionIndex.x + 1, _ball.PositionIndex.y));
                break;
            }
            
            case Direction.Down:
            {
                index = GetBallIndexByPosition(new Vector2Int(_ball.PositionIndex.x, _ball.PositionIndex.y + 1));
                break;
            }

            case Direction.Up:
            {
                index = GetBallIndexByPosition(new Vector2Int(_ball.PositionIndex.x, _ball.PositionIndex.y - 1));
                break;
            }
        }

        return index >= 0 && !balls[index].Removed ? balls[index] : null;
    }

    /// <summary>
    /// Находит все матчи
    /// </summary>
    /// <param name="_result"></param>
    /// <returns></returns>
    public bool FindMatch(out List<Ball> _result)
    {
        _result = new List<Ball>();
        if (balls == null)
            return false;

        for (int i = 0; i < balls.Length; i++)
        {
            List<Ball> match = new List<Ball> { balls[i]};
            //проверяем вправо
            Ball next = GetNeighbour(balls[i], Direction.Right);
            while (next != null && next.Letter == balls[i].Letter)
            {
                match.Add(next);
                next = GetNeighbour(next, Direction.Right);
            }

            if (match.Count >= 3)
            {
                foreach (Ball ball in match)
                {
                    _result.AddOnce(ball);
                }
            }

            match = new List<Ball> { balls[i]};
            //проверяем вниз
            next = GetNeighbour(balls[i], Direction.Down);
            while (next != null && next.Letter == balls[i].Letter)
            {
                match.Add(next);
                next = GetNeighbour(next, Direction.Down);
            }

            if (match.Count >= 3)
            {
                foreach (Ball ball in match)
                {
                    _result.AddOnce(ball);
                }
            }

        }

        return _result.Count > 0;
    }

    /// <summary>
    /// Поменять местами два шарика
    /// </summary>
    /// <param name="_ball1"></param>
    /// <param name="_ball2"></param>
    /// <param name="_onFinished"></param>
    public void Change(Ball _ball1, Ball _ball2, Action _onFinished = null)
    {
        Vector2Int temp = _ball1.PositionIndex;
        _ball1.SetPosition(_ball2.PositionIndex);
        _ball2.SetPosition(temp, _onFinished);
    }

    /// <summary>
    /// Находит первый потенциальный матч
    /// </summary>
    /// <param name="_match"></param>
    /// <returns></returns>
    public bool FindPotentialMatch(out List<Ball> _result)
    {
        _result = new List<Ball>();
        if (balls == null)
            return false;

        Vector2Int[] horizTemplatesLeft = 
        {
            new Vector2Int(-2, 0), new Vector2Int(-1, 1), new Vector2Int(-1, -1)
        };
        Vector2Int[] horizTemplatesRight = 
        {
            new Vector2Int(2, 0), new Vector2Int(1, 1), new Vector2Int(1, -1)
        };

        Vector2Int[] vertTemplatesUp = 
        {
            new Vector2Int(0, -2), new Vector2Int(-1, -1), new Vector2Int(1, -1)
        };
        Vector2Int[] vertTemplatesDown = 
        {
            new Vector2Int(0, 2), new Vector2Int(1, 1), new Vector2Int(-1, 1)
        };
        
        
        List<Ball[]> horizontalMatches = new List<Ball[]>();
        List<Ball[]> verticalMatches = new List<Ball[]>();

        for (int i = 0; i < balls.Length; i++)
        {
            //проверяем вправо
            Ball right = GetNeighbour(balls[i], Direction.Right);
            if (right != null && balls[i].Letter == right.Letter)
            {
                horizontalMatches.Add(new[] {balls[i], right});
            }

            //проверяем вниз
            Ball down = GetNeighbour(balls[i], Direction.Down);
            if (down != null && balls[i].Letter == down.Letter)
            {
                verticalMatches.Add(new[] {balls[i], down});
            }
        }
        
        //проверяем горизонтальные матчи
        foreach (Ball[] horizontalMatch in horizontalMatches)
        {
            foreach (Vector2Int horizTemplate in horizTemplatesLeft)
            {
                int index = GetBallIndexByPosition(new Vector2Int(horizontalMatch[0].PositionIndex.x + horizTemplate.x, horizontalMatch[0].PositionIndex.y + horizTemplate.y));
                if (index > 0 && balls[index].Letter == horizontalMatch[0].Letter)
                {
                    _result.Add(balls[index]);
                    _result.AddRange(horizontalMatch);
                    return true;
                }
            }
            
            foreach (Vector2Int horizTemplate in horizTemplatesRight)
            {
                int index = GetBallIndexByPosition(new Vector2Int(horizontalMatch[1].PositionIndex.x + horizTemplate.x, horizontalMatch[1].PositionIndex.y + horizTemplate.y));
                if (index > 0 && balls[index].Letter == horizontalMatch[1].Letter)
                {
                    _result.Add(balls[index]);
                    _result.AddRange(horizontalMatch);
                    return true;
                }
            }
        }
        
        //проверяем вертикальные матчи
        foreach (Ball[] verticalMatch in verticalMatches)
        {
            foreach (Vector2Int vertTemplate in vertTemplatesUp)
            {
                int index = GetBallIndexByPosition(new Vector2Int(verticalMatch[0].PositionIndex.x + vertTemplate.x, verticalMatch[0].PositionIndex.y + vertTemplate.y));
                if (index > 0 && balls[index].Letter == verticalMatch[0].Letter)
                {
                    _result.Add(balls[index]);
                    _result.AddRange(verticalMatch);
                    return true;
                }
            }
            
            foreach (Vector2Int vertTemplate in vertTemplatesDown)
            {
                int index = GetBallIndexByPosition(new Vector2Int(verticalMatch[1].PositionIndex.x + vertTemplate.x, verticalMatch[1].PositionIndex.y + vertTemplate.y));
                if (index > 0 && balls[index].Letter == verticalMatch[1].Letter)
                {
                    _result.Add(balls[index]);
                    _result.AddRange(verticalMatch);
                    return true;
                }
            }
        }

        return false;
    }
}
