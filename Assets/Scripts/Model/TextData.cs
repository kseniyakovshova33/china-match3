using System;
using UnityEngine;

[Serializable]
public class TextData
{
    [SerializeField]
    private string text;

    public string Text => text;

    [SerializeReference]
    private Condition showCondition;

    public Condition ShowCondition => showCondition;

    [SerializeField]
    private bool onlyOnce;

    public static Action<string> OnShown;
    
    public bool Shown { get; set; }

    public void Init()
    {
        showCondition.OnUnlock += Show;
        showCondition.Init();
    }

    public void Show()
    {
        if (!Shown && !string.IsNullOrEmpty(text))
        {
            OnShown.Execute(text);
            if (onlyOnce)
                Shown = true;
        }
    }
}
