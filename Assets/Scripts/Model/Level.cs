using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

/// <summary>
/// Уровень
/// </summary>
[Serializable]
public class Level
{
    [SerializeField]
    private int number;

    public int Number => number;
    
    [SerializeField] private string name;

    public string Name => name;

    [SerializeField] private Sprite image;

    public Sprite Image => image;

    [SerializeField] private int price;

    public int Price => price;

    [SerializeField] private LevelType levelType;

    public LevelType LevelType => levelType;

    private bool IsExam => levelType == LevelType.Exam || levelType == LevelType.SuperExam;
    private bool IsTraning => levelType == LevelType.Training;

    /// <summary>
    /// Иероглифы уровня
    /// </summary>
    [SerializeField] [ShowIf("IsTraning")]
    private LetterData[] letters;
    
    /// <summary>
    /// С каких уровней берем буквы
    /// </summary>
    [SerializeField] [ShowIf("IsExam")]
    private int[] levels;

    public LetterData[] Letters
    {
        get
        {
            if (IsTraning) return letters;
            List<LetterData> result = new List<LetterData>();
            foreach (int level in levels)
            {
                result.AddRange(LevelManager.Instance.GetLevelByNumber(level).letters);
            }

            return result.ToArray();
        }
    }

    [SerializeField] private SubLevel[] subLevels;

    public SubLevel[] SubLevels => subLevels;

    [SerializeField]
    private bool resetScoreOnUnlock;

    public bool Unlocked { get; private set; }

    public bool Completed { get; set; }

    public void Unlock()
    {
        if (resetScoreOnUnlock)
            Player.Instance.Score = 0;
        else Player.Instance.Score -= Price;
        Unlocked = true;
    }

    public void Save()
    {
        PlayerPrefs.SetInt("u_lvl" + number, Unlocked ? 1 : 0);
        PlayerPrefs.SetInt("c_lvl" + number, Completed ? 1 : 0);
        PlayerPrefs.Save();
    }

    public void Load()
    {
        Unlocked = PlayerPrefs.GetInt("u_lvl" + number, 0) == 1 || price == 0;
        Completed = PlayerPrefs.GetInt("c_lvl" + number, 0) == 1;
    }
}

public enum LevelType
{
    Training,
    Exam,
    SuperExam
}
