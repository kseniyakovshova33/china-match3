using UnityEngine;

public class LevelEndedCondition : Condition
{
    [SerializeField]
    private int level;

    public int Level => level;

    /// <summary>
    /// Условие выполнено, если следующий уровень недоступен
    /// </summary>
    [SerializeField]
    private bool ifNextLevelDisabled;

    public bool IfNextLevelDisabled => ifNextLevelDisabled;

    public override void Init()
    {
        GameManager.OnEndLevel -= OnEndLEvel;
        GameManager.OnEndLevel += OnEndLEvel;
    }

    private void OnEndLEvel(Level _level)
    {
        if (_level.Number == level)
        {
            if (ifNextLevelDisabled)
            {
                if (_level.Number == Player.Instance.UnlockedLevel)
                {
                    Level nextLevel = LevelManager.Instance.GetLevelByNumber(_level.Number + 1);
                    if (nextLevel != null && nextLevel.Price > Player.Instance.Score)
                    {
                        OnUnlock.Execute();
                    }
                }
            }
            else if (_level.Number + 1 == Player.Instance.UnlockedLevel || //уже открыт
                     (LevelManager.Instance.GetLevelByNumber(_level.Number + 1) != null &&
                      LevelManager.Instance.GetLevelByNumber(_level.Number + 1).Price <= Player.Instance.Score)) //или может быть открыт
            {
                OnUnlock.Execute();
            }
        }
    }
}

