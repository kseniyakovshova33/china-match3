using System;

public abstract class Condition
{
    public Action OnUnlock;

    public abstract void Init();
}
