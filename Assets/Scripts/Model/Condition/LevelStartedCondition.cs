using UnityEngine;

public class LevelStartedCondition : Condition
{
    [SerializeField]
    private int level;

    public override void Init()
    {
        GameManager.OnStartLevel -= OnStartLevel;
        GameManager.OnStartLevel += OnStartLevel;
    }

    private void OnStartLevel(Level _level)
    {
        if (_level.Number == level)
        {
            OnUnlock.Execute();
        }
    }
}
