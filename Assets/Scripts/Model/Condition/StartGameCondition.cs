using System.Collections;
using UnityEngine;

public class StartGameCondition : Condition
{
    public override void Init()
    {
        if (Player.Instance.CompletedLevel < 1)
            Player.Instance.StartCoroutine(Unlock());
    }

    private IEnumerator Unlock()
    {
        yield return new WaitForSeconds(0.5f);
        OnUnlock.Execute();
    }
}
